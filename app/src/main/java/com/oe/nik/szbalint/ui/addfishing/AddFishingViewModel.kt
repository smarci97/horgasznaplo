package com.oe.nik.szbalint.ui.addfishing

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import com.oe.nik.szbalint.database.FishingDatabase
import com.oe.nik.szbalint.database.FishingRepository
import com.oe.nik.szbalint.models.Fishing
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AddFishingViewModel(application: Application) : AndroidViewModel(application) {

    private var repository : FishingRepository

    init {
        val fishingDao = FishingDatabase.getInstance(application).fishingDao
        repository = FishingRepository(fishingDao)
    }

    fun saveToDatabase(fishing : Fishing) = GlobalScope.launch {
        repository.insertFishing(fishing)
    }

}