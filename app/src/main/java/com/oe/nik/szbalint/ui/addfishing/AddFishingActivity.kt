package com.oe.nik.szbalint.ui.addfishing

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.lifecycle.ViewModelProvider
import com.oe.nik.szbalint.R
import com.oe.nik.szbalint.models.Fishing

class AddFishingActivity : AppCompatActivity() {

    private lateinit var addFishingViewModel: AddFishingViewModel
    private lateinit var et_location: AppCompatEditText
    private lateinit var et_feeder: AppCompatEditText
    private lateinit var et_startTime: AppCompatEditText
    private lateinit var et_endTime: AppCompatEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addfishing)

        addFishingViewModel = ViewModelProvider(this).get(AddFishingViewModel::class.java)

        initViewElements()
        handleClickEvents()
    }

    private fun handleClickEvents() {
        findViewById<AppCompatButton>(R.id.save_btn_fishing)
            .setOnClickListener {
                saveToDatabase()
            }
    }

    private fun saveToDatabase() {
        addFishingViewModel.saveToDatabase(
            Fishing(
                location = et_location.text.toString(),
                feeder = et_feeder.text.toString(),
                start_time = et_startTime.text.toString(),
                end_time = et_endTime.text.toString()
            )
        )
        Toast.makeText(this, "Fishing is added to database.", Toast.LENGTH_SHORT).show()
        this.finish()
    }

    private fun initViewElements() {
        et_location = findViewById<AppCompatEditText>(R.id.et_location)
        et_feeder = findViewById<AppCompatEditText>(R.id.et_feeder)
        et_startTime = findViewById<AppCompatEditText>(R.id.et_startTime)
        et_endTime = findViewById<AppCompatEditText>(R.id.et_endTime)
    }
}