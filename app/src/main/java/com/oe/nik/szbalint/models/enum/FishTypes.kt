package com.oe.nik.szbalint.models.enum

enum class FishTypes {
    AMUR,
    BALIN,
    BUSA,
    CSUKA,
    DEVERKESZEG,
    EZUSTKARASZ,
    FOGASSULO,
    GARDA,
    HARCSA,
    KARIKAKESZEG,
    MARNA,
    PADUC,
    PONTY
}