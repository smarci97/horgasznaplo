package com.oe.nik.szbalint.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.oe.nik.szbalint.database.dao.FishingDao
import com.oe.nik.szbalint.models.Catch
import com.oe.nik.szbalint.models.Fishing

@Database(
    entities = [
        Fishing::class,
        Catch::class,
    ],
    version = 1
)
abstract class FishingDatabase : RoomDatabase()
{
    abstract val fishingDao: FishingDao

    companion object
    {
        @Volatile
        private var INSTANCE: FishingDatabase? = null

        fun getInstance(context: Context): FishingDatabase
        {
            synchronized(this)
            {
                return INSTANCE ?: Room.databaseBuilder(
                    context.applicationContext,
                    FishingDatabase::class.java,
                    "fishing_db"
                ).build().also {INSTANCE = it}
            }
        }
    }
}