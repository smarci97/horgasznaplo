package com.oe.nik.szbalint.ui.addcatch

import android.app.Application
import android.view.View
import android.widget.AdapterView
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.oe.nik.szbalint.database.FishingDatabase
import com.oe.nik.szbalint.database.FishingRepository
import com.oe.nik.szbalint.models.Catch
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AddCatchViewModel(application: Application) : AndroidViewModel(application) {

    private var repository: FishingRepository

    init {
        val fishingDao = FishingDatabase.getInstance(application).fishingDao
        repository = FishingRepository(fishingDao)
    }

    fun saveToDatabase(catch : Catch) = GlobalScope.launch {
        repository.insertCatch(catch)
    }
}