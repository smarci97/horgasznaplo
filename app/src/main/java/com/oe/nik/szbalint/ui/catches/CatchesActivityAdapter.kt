package com.oe.nik.szbalint.ui.catches

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.oe.nik.szbalint.databinding.RecyclerItemCatchModelBinding
import com.oe.nik.szbalint.models.Catch

class CatchesActivityAdapter(
    private var catchesList: MutableList<Catch>
) : RecyclerView.Adapter<CatchesActivityAdapter.VH>() {

    var onLongClick : ((Catch) -> Unit)? = null

    fun swapData(catchesList: MutableList<Catch>) {
        clearDataFromTheLists()
        addDataToTheLists(catchesList)
        notifyDataSetChanged()
    }

    private fun clearDataFromTheLists() {
        this.catchesList.clear()
    }

    private fun addDataToTheLists(catchesList: MutableList<Catch>) {
        this.catchesList.addAll(catchesList)
    }

    inner class VH(
        val binding: RecyclerItemCatchModelBinding
    ) : RecyclerView.ViewHolder(binding.root){
        fun bind(item : Catch){
            binding.catchModel = item
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val binding = RecyclerItemCatchModelBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return VH(binding)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(catchesList[position])

        holder.binding.clCatchContainer.setOnLongClickListener {
            onLongClick?.invoke(catchesList[position])
            true
        }
    }

    override fun getItemCount(): Int = catchesList.size
}