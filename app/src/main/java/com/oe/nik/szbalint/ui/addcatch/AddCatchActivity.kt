package com.oe.nik.szbalint.ui.addcatch

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.lifecycle.ViewModelProvider
import com.oe.nik.szbalint.R
import com.oe.nik.szbalint.models.Catch

class AddCatchActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private lateinit var addCatchViewModel: AddCatchViewModel
    private lateinit var weight: AppCompatEditText
    private lateinit var length: AppCompatEditText
    private lateinit var bait: AppCompatEditText

    //todo: catch_date is a date not a string
    private lateinit var catch_date: AppCompatEditText
    private lateinit var spinner: Spinner
    private var species: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addcatch)

        addCatchViewModel = ViewModelProvider(this).get(AddCatchViewModel::class.java)

        initViewElements()
        createSpinner()
        spinner.onItemSelectedListener = this
        handleClickEvents()
    }

    private fun handleClickEvents() {
        findViewById<AppCompatButton>(R.id.save_btn_catch)
            .setOnClickListener {
                saveToDatabase()
            }
    }

    private fun saveToDatabase() {
        if (species != null) {
            addCatchViewModel.saveToDatabase(
                Catch(
                    id_fishing = intent.extras!!.getLong("fishing_id"),
                    weight = weight.text.toString(),
                    length = length.text.toString(),
                    bait = bait.text.toString(),
                    species = species!!,
                    catch_date = catch_date.text.toString()
                )
            )
            Toast.makeText(this, "Fishing is added to database.", Toast.LENGTH_SHORT).show()
            this.finish()
        } else{
            Toast.makeText(this, "Pick a specie.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun initViewElements() {
        weight = findViewById<AppCompatEditText>(R.id.et_weight)
        length = findViewById<AppCompatEditText>(R.id.et_length)
        bait = findViewById<AppCompatEditText>(R.id.et_bait)
        catch_date = findViewById<AppCompatEditText>(R.id.et_catchdate)
        spinner = findViewById<Spinner>(R.id.spin_species)
    }

    fun createSpinner() {
        ArrayAdapter.createFromResource(
            this,
            R.array.fishTypes,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
        species = parent?.getItemAtPosition(pos).toString()
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        species = null
    }
}
