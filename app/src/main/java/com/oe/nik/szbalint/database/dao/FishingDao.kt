package com.oe.nik.szbalint.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.oe.nik.szbalint.models.Catch
import com.oe.nik.szbalint.models.Fishing
import com.oe.nik.szbalint.models.relation.FishingWithCatches

@Dao
interface FishingDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFishing(fishing: Fishing)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCatch(catch: Catch)

    @Delete
    suspend fun deleteFishing(item: Fishing)

    @Delete
    suspend fun deleteCatch(item: Catch)


    //egy fishing-et ad vissza, nem a hozzá tartozo catch-eket
    // from helyére catch_table
    @Transaction
    @Query("SELECT * FROM catch_table WHERE id_fishing = :id_fishing")
    fun getCatchesOfFishing(id_fishing: Long): LiveData<MutableList<Catch>>

    @Transaction
    @Query("SELECT * FROM fishing_table")
    fun getAllFishing(): LiveData<MutableList<FishingWithCatches>>
}