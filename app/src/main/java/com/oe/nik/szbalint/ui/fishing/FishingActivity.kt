package com.oe.nik.szbalint.ui.fishing

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

import com.oe.nik.szbalint.R
import com.oe.nik.szbalint.databinding.ActivityFishingBinding
import com.oe.nik.szbalint.models.relation.FishingWithCatches
import com.oe.nik.szbalint.ui.addfishing.AddFishingActivity
import com.oe.nik.szbalint.ui.catches.CatchesActivity

class FishingActivity : AppCompatActivity() {

    private lateinit var fishingViewModel: FishingViewModel
    private lateinit var fishingActivityAdapter: FishingActivityAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var fishingRecyclerView: RecyclerView
    private lateinit var fabAdd: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setAdapter()
        setViewModel()

        setView()

        loadRecyclerView()
        getFishingDataFromViewModel()
        setViewElements()
        handleClickEvents()
    }

    private fun setViewElements() {
        fabAdd = findViewById(R.id.fab_add_fishing)
    }

    private fun handleClickEvents() {
        fishingActivityAdapter.onClick = {
            startActivity(Intent(this, CatchesActivity::class.java).apply {
                putExtra("fishing_id", it.fishing.id_fishing)
            })
        }

        fishingActivityAdapter.onLongClick = {
            fishingViewModel.deleteFishing(it.fishing)
            Toast.makeText(this, "Item is deleted.", Toast.LENGTH_SHORT).show()
        }

        fabAdd.setOnClickListener {
            startActivity(Intent(this, AddFishingActivity::class.java))
        }
    }

    private fun setView() {
        DataBindingUtil.setContentView<ActivityFishingBinding>(this, R.layout.activity_fishing).apply {
            this.lifecycleOwner = this@FishingActivity
        }
    }

    private fun setAdapter() {
        fishingActivityAdapter = FishingActivityAdapter(mutableListOf())
    }

    private fun setViewModel() {
        fishingViewModel = ViewModelProvider(this).get(FishingViewModel::class.java)
    }

    private fun loadRecyclerView() {
        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        fishingRecyclerView = findViewById(R.id.fishing_recyclerview)
        fishingRecyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = fishingActivityAdapter
        }
    }

    private fun getFishingDataFromViewModel() {
        val fishingLiveData: LiveData<MutableList<FishingWithCatches>> =
            fishingViewModel.getAllFishingFromLocalDB()
        observeFishingDataToUi(fishingLiveData)
    }

    private fun observeFishingDataToUi(fishingLiveData: LiveData<MutableList<FishingWithCatches>>) {
        fishingLiveData.observe(
            this,
            Observer { fishingList -> fishingActivityAdapter.swapData(fishingList) })
    }
}