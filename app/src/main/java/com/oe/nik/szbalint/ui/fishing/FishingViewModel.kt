package com.oe.nik.szbalint.ui.fishing

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.oe.nik.szbalint.database.FishingDatabase
import com.oe.nik.szbalint.database.FishingRepository
import com.oe.nik.szbalint.database.dao.FishingDao
import com.oe.nik.szbalint.models.Fishing
import com.oe.nik.szbalint.models.relation.FishingWithCatches
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FishingViewModel(application: Application) : AndroidViewModel(application) {

    private var repository : FishingRepository

    init {
        val fishingDao = FishingDatabase.getInstance(application).fishingDao
        repository = FishingRepository(fishingDao)
    }

    fun getAllFishingFromLocalDB(): LiveData<MutableList<FishingWithCatches>> = repository.getAllFishing()


    fun deleteFishing(fishing : Fishing) = GlobalScope.launch {
        repository.deleteFishing(fishing)
    }
}