package com.oe.nik.szbalint.ui.catches

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.oe.nik.szbalint.database.FishingDatabase
import com.oe.nik.szbalint.database.FishingRepository
import com.oe.nik.szbalint.models.Catch
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CatchesViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: FishingRepository

    init {
        val fishingDao = FishingDatabase.getInstance(application).fishingDao
        repository = FishingRepository(fishingDao)
    }

    fun getCatchesOfFishing(fishing_id: Long): LiveData<MutableList<Catch>> {
        return repository.getCatchesOfFishing(fishing_id)
    }

    fun deleteCatch(catch: Catch) = GlobalScope.launch {
        repository.deleteCatch(catch)
    }
}