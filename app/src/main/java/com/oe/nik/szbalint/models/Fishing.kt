package com.oe.nik.szbalint.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "fishing_table")
data class Fishing(
    @PrimaryKey(autoGenerate = true)
    val id_fishing: Long=0L,
    var location: String = "",
    var feeder: String = "",
    var start_time: String= "",
    var end_time: String = "",
)

