package com.oe.nik.szbalint.models.relation

import androidx.room.Embedded
import androidx.room.Relation
import com.oe.nik.szbalint.models.Catch
import com.oe.nik.szbalint.models.Fishing

data class FishingWithCatches(
    @Embedded val fishing: Fishing,
    @Relation(
        parentColumn = "id_fishing",
        entityColumn = "id_fishing"
    )
    val catches: MutableList<Catch>
)
