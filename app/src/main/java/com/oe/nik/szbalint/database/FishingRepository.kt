package com.oe.nik.szbalint.database

import com.oe.nik.szbalint.database.dao.FishingDao
import com.oe.nik.szbalint.models.Catch
import com.oe.nik.szbalint.models.Fishing

class FishingRepository(private val fishingDao : FishingDao) {

    suspend fun insertFishing(item : Fishing) = fishingDao.insertFishing(item)
    
    suspend fun deleteFishing(item: Fishing) = fishingDao.deleteFishing(item)

    suspend fun insertCatch(item : Catch) = fishingDao.insertCatch(item)

    suspend fun deleteCatch(item: Catch) = fishingDao.deleteCatch(item)

    fun getAllFishing() = fishingDao.getAllFishing()

    fun getCatchesOfFishing(fishing_id : Long) = fishingDao.getCatchesOfFishing(fishing_id)
}