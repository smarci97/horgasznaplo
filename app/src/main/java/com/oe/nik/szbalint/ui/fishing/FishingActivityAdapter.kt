package com.oe.nik.szbalint.ui.fishing

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.oe.nik.szbalint.databinding.RecyclerItemFishingModelBinding
import com.oe.nik.szbalint.models.relation.FishingWithCatches

class FishingActivityAdapter(
    private var fishingList: MutableList<FishingWithCatches>
) : RecyclerView.Adapter<FishingActivityAdapter.VH>() {

    var onClick :((FishingWithCatches) -> Unit)? = null
    var onLongClick :((FishingWithCatches) -> Unit)? = null

    fun swapData(fishingList: MutableList<FishingWithCatches>) {
        clearDataFromTheLists()
        addDataToTheLists(fishingList)
        notifyDataSetChanged()
    }

    private fun clearDataFromTheLists() {
        this.fishingList.clear()
    }

    private fun addDataToTheLists(fishingList: MutableList<FishingWithCatches>) {
        this.fishingList.addAll(fishingList)
    }

    inner class VH(val binding: RecyclerItemFishingModelBinding) :
        RecyclerView.ViewHolder(binding.root){
            fun bind(item : FishingWithCatches){
                binding.fishingModel = item
                binding.executePendingBindings()
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val binding = RecyclerItemFishingModelBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return VH(binding)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(fishingList[position])

        holder.binding.clFishingContainer.setOnClickListener {
            onClick?.invoke(fishingList[position])
        }

        holder.binding.clFishingContainer.setOnLongClickListener {
            onLongClick?.invoke(fishingList[position])
            true
        }
    }

    override fun getItemCount(): Int = fishingList.size
}