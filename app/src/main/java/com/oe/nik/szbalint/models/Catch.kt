package com.oe.nik.szbalint.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "catch_table")
data class Catch(
    @PrimaryKey(autoGenerate = true)
    val id_catch: Long=0L,
    val id_fishing: Long,
    var weight: String = "",
    var length: String = "",
    var bait: String= "",
    var catch_date: String = "",
    var species: String = "")
