package com.oe.nik.szbalint.ui.catches

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.oe.nik.szbalint.R
import com.oe.nik.szbalint.models.Catch
import com.oe.nik.szbalint.ui.addcatch.AddCatchActivity
import com.oe.nik.szbalint.ui.addfishing.AddFishingActivity

class CatchesActivity : AppCompatActivity() {

    private lateinit var catchesViewmodel: CatchesViewModel
    private lateinit var catchesActivityAdapter: CatchesActivityAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var catchesRecyclerView: RecyclerView
    private lateinit var fabAdd: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_catches)
        fabAdd = findViewById(R.id.fab_add_catch)
        setAdapter()
        setViewModel()
        loadRecyclerView()
        getCatchesDataFromViewModel()

        handleOnClicks()
    }

    private fun handleOnClicks() {
        catchesActivityAdapter.onLongClick = {
            catchesViewmodel.deleteCatch(it)
            Toast.makeText(this, "Item is deleted.", Toast.LENGTH_SHORT).show()
        }

        fabAdd.setOnClickListener {
            startActivity(Intent(this, AddCatchActivity::class.java).apply {
                putExtra("fishing_id",intent.extras!!.getLong("fishing_id"))
            })
        }
    }

    private fun setAdapter() {
        catchesActivityAdapter = CatchesActivityAdapter(mutableListOf())
    }

    private fun setViewModel() {
        catchesViewmodel = ViewModelProvider(this).get(CatchesViewModel::class.java)
    }

    private fun loadRecyclerView() {
        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        catchesRecyclerView = findViewById(R.id.catches_recyclerview)
        catchesRecyclerView.apply {
            layoutManager = linearLayoutManager
            adapter = catchesActivityAdapter
        }
    }

    private fun getCatchesDataFromViewModel() {
        val catchesLiveData: LiveData<MutableList<Catch>> =
            catchesViewmodel.getCatchesOfFishing(intent.extras!!.getLong("fishing_id"))
        observeFishingDataToUi(catchesLiveData)
    }

    private fun observeFishingDataToUi(catchesLiveData: LiveData<MutableList<Catch>>) {
        catchesLiveData.observe(
            this,
            Observer { catchesList -> catchesActivityAdapter.swapData(catchesList) })
    }
}